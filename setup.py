from setuptools import setup

setup(
    name='ktty',
    py_modules=['ktty'],
    install_requires=[
        'flask',
    ],
)
