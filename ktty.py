import ConfigParser
import os
import random

from flask import Flask, request, redirect, url_for, send_from_directory
from werkzeug import secure_filename

__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))

config = ConfigParser.ConfigParser()
config.read(os.path.join(__location__, 'ktty.cat.ini'))

UPLOAD_FOLDER = config.get('DEFAULT', 'upload_folder')
ALLOWED_EXTENSIONS = {'webm'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 100 * 1024 * 1024 # 100MB max upload size

current_file_list = []

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            path = os.path.join(app.config['UPLOAD_FOLDER'],
                                'staging',
                                filename)
            if not os.path.isfile(path):
                file.save(path)
                return redirect(url_for('webm_staging',
                                filename=filename.replace('.webm', '')))
            else:
                return '''That filename already exists
TODO: add some nicer message here'''

    return '''
<html>
<link rel="stylesheet"
href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
crossorigin="anonymous">

<link rel="stylesheet"
href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r"
crossorigin="anonymous">

    <title>Add new webm</title>
    <h1>Upload webm to staging</h1>
    <form action="" method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload></p>
    </form>
</html>
    '''

def show_webm(filename):
    path = filename.replace('.webm', '')
    title = 'ktty.cat - {}'.format(path)
    if not filename.endswith('.webm'):
        filename = filename + '.webm'
    return '''
<html>
    <style>
        body {{
            margin: 0;
            background-color: #0B0A17;
        }}

        video#bgvid {{
            width: 100%;
            height: 100%;
            -webkit-flex:0 0 auto;
            -ms-flex:0 0 auto;
            flex: 0 0 auto;
        }}

        body {{
            -moz-box-shadow: inset 0 0 10em black;
             -webkit-box-shadow: inset 0 0 10em black;
             box-shadow: inset 0 0 10em black;
             position: absolute;
             top: 0;
             left: 0;
             width: 100%;
             height: 100%;
             z-index: 2;
             content: "";
        }}
    </style>
    <head>
        <title>{title}</title>
    </head>
    <body style="overflow: hidden">
        <a href="/{path}">
            <video autoplay loop id="bgvid">
                <source src="{filename}" type="video/webm">
            </video>
        </a>
    </body>
</html>
'''.format(filename=filename, title=title, path=path)

@app.route('/<filename>')
def webm(filename):
    if filename.endswith('.webm'):
        return send_from_directory(app.config['UPLOAD_FOLDER'],
                                   filename)
    else:
        return show_webm(filename)

@app.route('/staging/<filename>')
def webm_staging(filename):
    if filename.endswith('.webm'):
        return send_from_directory(os.path.join(app.config['UPLOAD_FOLDER'],
                                                'staging'),
                                   filename)
    else:
        return show_webm(filename)

def totally_random_webm():
    global current_file_list
    if not current_file_list:
        current_file_list = list(filter(lambda f: f.endswith('.webm'),
                                         os.listdir(app.config['UPLOAD_FOLDER'])
        ))
        current_file_list = current_file_list[0:int((2/3.0) * len(current_file_list))]
    filename = random.choice(current_file_list)
    current_file_list.remove(filename)
    return filename


@app.route('/staging')
def ktty_staging():
    filename = random.choice(os.listdir(os.path.join(
                                        app.config['UPLOAD_FOLDER'],
                                        'staging')
                                       ))
    return show_webm(os.path.join('staging', filename))

@app.route('/')
def ktty():
    filename = totally_random_webm()
    return show_webm(filename)

if __name__ == '__main__':
    app.run(debug=True)
